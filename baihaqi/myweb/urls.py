from django.urls import path
from . import views

urlpatterns = [
    path('/',views.haloworld,name='haloworld'),
    path('hibai/',views.hibai,name='hibai'),
    path('sekuy/',views.sekuy,name='sekuy'),
    path('help/',views.help,name='help'),
]